import React from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';
import { movieService } from "../../services/movie-service";
import MovieCard from '../../shared/movie-card/movie-card';

export default class DetailsScreen extends React.Component {
    constructor(props){
      super(props);

      var { getParam } = this.props.navigation;
      this.state = {
        movie: {},
        loading: true,
        page: 1,
        id: getParam('id', 0)
      }
    }

    componentDidMount() {
      movieService.getDetails(this.state.id).then(movie => {
        this.setState({
          movie
        })
      })
    }

    static navigationOptions = {
        title: 'Home Screen'
    };
  
    render() {
      return (
        <View style={styles.container}>
          <MovieCard
                movie={this.state.movie}>
            </MovieCard>
        </View>
      );
    }
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      paddingTop: 16
    },
  });