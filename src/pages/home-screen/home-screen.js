import React from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';
import MovieCard from "../../shared/movie-card/movie-card";
import { movieService } from "../../services/movie-service";

export default class HomeScreen extends React.Component {
    static navigationOptions = {
        title: 'Home Screen'
    };

    state = {
        movies: [],
        loading: true,
        page: 1
    }

    loadMovies() {
        movieService.getMovies(this.state.page).then(response => {
            var movies = this.state.movies;
            movies = movies.concat(response.map((movie, index) => {
                return {
                    ...movie,
                    key: movie.id.toString()
                }
            }));

            this.setState({
                movies,
                loading: false
            });
        })
    }

    componentDidMount() {
        this.loadMovies();
    }

    cardClicked(id) {
        const { navigate } = this.props.navigation;
        navigate('Details', {
            id: id
        })
    }

    getMovieCardComponent(item) {
        return (
            <MovieCard
                movie={item}
                onCardClicked={({ id }) => this.cardClicked(id)}>
            </MovieCard>
        );
    }

    endReached() {
        this.setState(previousState => {
            return {
                page: previousState.page + 1
            }
        }, () => {
            this.loadMovies();
        })
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                {
                    this.state.loading ? <Text>Caregando ...</Text> :
                        <FlatList
                            onEndReached={() => {
                                this.endReached();
                            }}
                            data={this.state.movies}
                            renderItem={({ item }) => 
                            <MovieCard
                                movie={item}
                                onCardClicked={(movie) => this.cardClicked(movie.id)}>
                            </MovieCard>}
                        />
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingTop: 16
    },
});