import axios from "axios";

export const movieService = {
    getMovies: (page = 1) => {
        return new Promise((resolve, reject) => {
            axios.get(`https://api.themoviedb.org/3/movie/popular?api_key=51e4e9d52532d389174b5252cd99d33d&page=${page}`)
            .then(({data}) => {
                resolve(data.results);
            }).catch(err => {
                reject(err);
            })
        })
    },
    getDetails: (id) => {
        return new Promise((resolve, reject) => {
            axios.get(`https://api.themoviedb.org/3/movie/${id}?api_key=51e4e9d52532d389174b5252cd99d33d`)
            .then((response) => {
                resolve(response.data);
            }).catch(err => {
                reject(err);
            })
        })
    }
}