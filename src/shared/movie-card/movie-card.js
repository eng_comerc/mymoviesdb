import React from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import { styles } from "./movie-card.style";

export default class MovieCard extends React.Component {
  render() {
    const { movie } = this.props;
    const image = `https://image.tmdb.org/t/p/w500/${movie.poster_path}`;
    //const movie = this.props.movie;

    return (
      <View style={ styles.container }>
        <View style={styles.imageView}>
          <TouchableOpacity onPress={() => {
            if(this.props.onCardClicked){
              this.props.onCardClicked(movie);
            }
          }}>
            <Image
              resizeMode={'cover'}
              style={{
                width: 400,
                height: 200
              }}
              source={{ uri: image }}
            />
          </TouchableOpacity>
        </View>

        { movie.tagline ? 
        <View>
          <Text>{movie.tagline}</Text>
        </View> : null }

        <View style={styles.titleInfo}>
          <Text style={styles.title}>{movie.title}</Text>
          <Text style={styles.date}>{movie.release_date}</Text>
        </View>
      </View>
    );
  }
}
