import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: .6,
        paddingBottom: 30
    },
    imageView: {
    },
    titleInfo: {
        
    },
    title: {
        fontSize: 22,
        textAlign: "center"
    },
    date: {
        textAlign: "center"
    }
});