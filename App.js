import { createStackNavigator } from 'react-navigation';
import HomeScreen from './src/pages/home-screen/home-screen';
import DetailsScreen from './src/pages/home-screen/details-screen';

const App = createStackNavigator({
  Home: { screen: HomeScreen },
  Details: { screen: DetailsScreen }
});

export default App;